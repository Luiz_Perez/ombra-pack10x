local config = {
	requiredLevel = 100,
	daily = false,
	playerPositions = {
		Position(33010, 31660, 14),
		Position(33010, 31661, 14),
		Position(33010, 31662, 14),
		Position(33010, 31663, 14),
		Position(33010, 31664, 14)
	},
	newPositions = {
		Position(32974, 31670, 14),
		Position(32975, 31670, 14),
		Position(32976, 31670, 14),
		Position(32977, 31670, 14),
		Position(32978, 31670, 14)
	},
	BossPositions = {
		Position(32977, 31658, 14)
	}
}


function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if item.itemid == 1946 then
		local storePlayers, playerTile = {}

		for i = 1, #config.playerPositions do
			playerTile = Tile(config.playerPositions[i]):getTopCreature()
			if not playerTile or not playerTile:isPlayer() then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "You need 5 players.")
				return true
			end

			if playerTile:getLevel() < config.requiredLevel then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "All the players need to be level ".. config.requiredLevel .." or higher.")
				return true
			end

			storePlayers[#storePlayers + 1] = playerTile
		end

		for i = 1, #config.BossPositions do
			Game.createMonster("The Time Guardian", config.BossPositions[i])
		end

		local players
		for i = 1, #storePlayers do
			players = storePlayers[i]
			config.playerPositions[i]:sendMagicEffect(CONST_ME_POFF)
			players:teleportTo(config.newPositions[i])
			config.newPositions[i]:sendMagicEffect(CONST_ME_ENERGYAREA)
			players:setDirection(DIRECTION_EAST)
		end
	elseif item.itemid == 1945 then
		if config.daily then
			player:sendTextMessage(MESSAGE_STATUS_SMALL, Game.getReturnMessage(RETURNVALUE_NOTPOSSIBLE))
			return true
		end
	end

	item:transform(item.itemid == 1946 and 1945 or 1946)
	return true
end