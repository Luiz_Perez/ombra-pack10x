local config = {
	requiredLevel = 100,
	daily = false,
	playerPositions = {
		Position(32902, 31623, 14),
		Position(32902, 31624, 14),
		Position(32902, 31625, 14),
		Position(32902, 31626, 14),
		Position(32902, 31627, 14)
	},
	newPositions = {
		Position(32910, 31603, 14),
		Position(32911, 31603, 14),
		Position(32912, 31603, 14),
		Position(32913, 31603, 14),
		Position(32914, 31603, 14)
	},
	BossPositions = {
		Position(32912, 31599, 14)
	}
}


function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if item.itemid == 1946 then
		local storePlayers, playerTile = {}

		for i = 1, #config.playerPositions do
			playerTile = Tile(config.playerPositions[i]):getTopCreature()
			if not playerTile or not playerTile:isPlayer() then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "You need 5 players.")
				return true
			end

			if playerTile:getLevel() < config.requiredLevel then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "All the players need to be level ".. config.requiredLevel .." or higher.")
				return true
			end

			storePlayers[#storePlayers + 1] = playerTile
		end

		for i = 1, #config.BossPositions do
			Game.createMonster("Lady Tenebris", config.BossPositions[i])
		end

		local players
		for i = 1, #storePlayers do
			players = storePlayers[i]
			config.playerPositions[i]:sendMagicEffect(CONST_ME_POFF)
			players:teleportTo(config.newPositions[i])
			config.newPositions[i]:sendMagicEffect(CONST_ME_ENERGYAREA)
			players:setDirection(DIRECTION_EAST)
		end
	elseif item.itemid == 1945 then
		if config.daily then
			player:sendTextMessage(MESSAGE_STATUS_SMALL, Game.getReturnMessage(RETURNVALUE_NOTPOSSIBLE))
			return true
		end
	end

	item:transform(item.itemid == 1946 and 1945 or 1946)
	return true
end