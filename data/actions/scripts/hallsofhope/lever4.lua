local config = {
	requiredLevel = 100,
	daily = false,
	playerPositions = {
		Position(32657, 32877, 14),
		Position(32657, 32878, 14),
		Position(32657, 32879, 14),
		Position(32657, 32880, 14),
		Position(32657, 32881, 14)
	},
	newPositions = {
		Position(32622, 32883, 14),
		Position(32623, 32883, 14),
		Position(32624, 32883, 14),
		Position(32625, 32883, 14),
		Position(32626, 32883, 14)
	},
	BossPositions = {
		Position(32624, 32880, 14)
	}
}


function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if item.itemid == 1946 then
		local storePlayers, playerTile = {}

		for i = 1, #config.playerPositions do
			playerTile = Tile(config.playerPositions[i]):getTopCreature()
			if not playerTile or not playerTile:isPlayer() then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "You need 5 players.")
				return true
			end

			if playerTile:getLevel() < config.requiredLevel then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "All the players need to be level ".. config.requiredLevel .." or higher.")
				return true
			end

			storePlayers[#storePlayers + 1] = playerTile
		end

		for i = 1, #config.BossPositions do
			Game.createMonster("The Enraged Thorn Knight", config.BossPositions[i])
		end

		local players
		for i = 1, #storePlayers do
			players = storePlayers[i]
			config.playerPositions[i]:sendMagicEffect(CONST_ME_POFF)
			players:teleportTo(config.newPositions[i])
			config.newPositions[i]:sendMagicEffect(CONST_ME_ENERGYAREA)
			players:setDirection(DIRECTION_EAST)
		end
	elseif item.itemid == 1945 then
		if config.daily then
			player:sendTextMessage(MESSAGE_STATUS_SMALL, Game.getReturnMessage(RETURNVALUE_NOTPOSSIBLE))
			return true
		end
	end

	item:transform(item.itemid == 1946 and 1945 or 1946)
	return true
end