local config = {
	requiredLevel = 100,
	daily = false,
	playerPositions = {
		Position(33391, 31178, 10),
		Position(33391, 31179, 10),
		Position(33391, 31180, 10),
		Position(33391, 31181, 10),
		Position(33391, 31182, 10)
	},
	newPositions = {
		Position(33356, 31185, 10),
		Position(33357, 31185, 10),
		Position(33358, 31185, 10),
		Position(33359, 31185, 10),
		Position(33360, 31185, 10)
	},
	BossPositions = {
		Position(33358, 31181, 10)
	}
}


function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if item.itemid == 1946 then
		local storePlayers, playerTile = {}

		for i = 1, #config.playerPositions do
			playerTile = Tile(config.playerPositions[i]):getTopCreature()
			if not playerTile or not playerTile:isPlayer() then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "You need 5 players.")
				return true
			end

			if playerTile:getLevel() < config.requiredLevel then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "All the players need to be level ".. config.requiredLevel .." or higher.")
				return true
			end

			storePlayers[#storePlayers + 1] = playerTile
		end

		for i = 1, #config.BossPositions do
			Game.createMonster("Soul of Dragonking Zyrtarch", config.BossPositions[i])
		end

		local players
		for i = 1, #storePlayers do
			players = storePlayers[i]
			config.playerPositions[i]:sendMagicEffect(CONST_ME_POFF)
			players:teleportTo(config.newPositions[i])
			config.newPositions[i]:sendMagicEffect(CONST_ME_ENERGYAREA)
			players:setDirection(DIRECTION_EAST)
		end
	elseif item.itemid == 1945 then
		if config.daily then
			player:sendTextMessage(MESSAGE_STATUS_SMALL, Game.getReturnMessage(RETURNVALUE_NOTPOSSIBLE))
			return true
		end
	end

	item:transform(item.itemid == 1946 and 1945 or 1946)
	return true
end